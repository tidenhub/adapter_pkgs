# Adapter Packages

## Inhalt

- adapter_package: Beispiel Anwednung zur Nutzung von Planning Request Adapters
- safety_planning_request_adapters: Package für Planning Request Adapter Plugins
    - add_safety.cpp: Planning Request Adapter Plugins

## Einrichtung

1. Wenn nicht vorhanden, dann Einrichtung eines ROS Workspaces: http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment
2. Clonen bzw. kopieren von adapter_pkgs in den ROS Workspace.
3. Registrierung des Planning Request Adapter Plugins in der .launch File. Beispiel ompl_planning_pipeline.launch.xml:
```
  <arg name="planning_adapters" default="
       safety_planning_request_adapters/AddSafety
       default_planner_request_adapters/FixWorkspaceBounds
       default_planner_request_adapters/FixStartStateBounds
       default_planner_request_adapters/FixStartStateCollision
       default_planner_request_adapters/FixStartStatePathConstraints
       default_planner_request_adapters/AddTimeParameterization"
       />
  <!-- default_planner_request_adapters/ResolveConstraintFrames -->
  <arg name="start_state_max_bounds_error" value="0.1" />

  <param name="planning_plugin" value="$(arg planning_plugin)" />
```
4. Bauen des ROS Workspaces.
