#include <class_loader/class_loader.hpp>
#include <moveit/planning_request_adapter/planning_request_adapter.h>
#include <ros/console.h>

namespace safety_planning_request_adapters
{
    class AddSafety : public planning_request_adapter::PlanningRequestAdapter
    {
        double VELOCITY_LIMIT = 0.1;

    public:

        std::string getDescription() const override
        {
            return "Add Safety";
        }

        bool adaptAndPlan(const PlannerFn& planner, const planning_scene::PlanningSceneConstPtr& planning_scene,
                          const planning_interface::MotionPlanRequest& req, planning_interface::MotionPlanResponse& res,
                          std::vector<std::size_t>& added_path_index) const override
        {
            ROS_INFO("Safety Planning Request Adapter: Plugin loaded");

            bool result = planner(planning_scene, req, res);
            if (result && res.trajectory_) // Only run after planning
            {
                ROS_INFO("Safety Planning Request Adapter: Start plugin");

                robot_trajectory::RobotTrajectory& trajectory = *res.trajectory_;
                const robot_model::JointModelGroup* group = trajectory.getGroup();
                if (!group)
                {
                    ROS_ERROR("Safety Planning Request Adapter: It looks like the planner did not set the group the plan was computed for");
                    return false;
                }

                const std::vector<int>& idx = group->getVariableIndexList();
                const unsigned num_joints = group->getVariableCount();
                const unsigned num_points = trajectory.getWayPointCount();

                for (size_t p = 0; p < num_points; ++p) {

                    robot_state::RobotStatePtr waypoint = trajectory.getWayPointPtr(p);
                    trajectory.setWayPointDurationFromPrevious(p, trajectory.getWayPointDurationFromPrevious(p) / VELOCITY_LIMIT);
                    // ROS_INFO("ADAPTER PACKAGE: vel1: (%f,%f)",
                             // trajectory.getWayPointDurationFromStart(p),
                             // waypoint->getVariableVelocity(idx[num_joints-1]) * VELOCITY_LIMIT);

                    for (size_t j = 0; j < num_joints; ++j) {
                        waypoint->setVariableVelocity(idx[j], waypoint->getVariableVelocity(idx[j]) * VELOCITY_LIMIT);
                        waypoint->setVariableAcceleration(idx[j], waypoint->getVariableAcceleration(idx[j]) * VELOCITY_LIMIT * VELOCITY_LIMIT);
                    }
                }
            }
            ROS_INFO("Safety Planning Request Adapter: End plugin");
            return result;
        };
    };
}  // namespace safety_planning_request_adapters

CLASS_LOADER_REGISTER_CLASS(safety_planning_request_adapters::AddSafety, planning_request_adapter::PlanningRequestAdapter);
