#include "std_msgs/String.h"
#include <moveit/move_group_interface/move_group_interface.h>


void move_toCallback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("ADAPTER PACKAGE: Recevied message: '%s'", msg->data.c_str());

    static const std::string PLANNING_GROUP = "panda_arm";
    moveit::planning_interface::MoveGroupInterface group(PLANNING_GROUP);

    ros::Duration(3.0).sleep();

    group.setStartStateToCurrentState();
    group.setPlanningTime(5.0);

    // pose A
    geometry_msgs::Pose target_pose_1 = group.getCurrentPose().pose;
    target_pose_1.position.z = 0.4;
    target_pose_1.position.y = 0.4;
    target_pose_1.position.x = 0.8;
    target_pose_1.orientation.x = 0;
    target_pose_1.orientation.y = 0;
    target_pose_1.orientation.z = 0;
    target_pose_1.orientation.w = 1;

    // pose B
    geometry_msgs::Pose target_pose_2 = group.getCurrentPose().pose;
    target_pose_2.position.z = 0;
    target_pose_2.position.y = 0.3;
    target_pose_2.position.x = 0.5;
    target_pose_2.orientation.x = 0.4;
    target_pose_2.orientation.y = 0.7;
    target_pose_2.orientation.z = 0.4;
    target_pose_2.orientation.w = 1;

    std::vector<geometry_msgs::Pose> waypoints;
    std::string strA ("A");
    std::string strB ("B");
    if (strA == msg->data) {
        group.setPoseTarget(target_pose_1);
    } else if (strB == msg->data) {
        group.setPoseTarget(target_pose_2);
    } else {
        ROS_WARN("ADAPTER PACKAGE: Wrong message.");
        return;
    }

    group.move();
    ROS_INFO("ADAPTER PACKAGE: Finished movement.");

}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "adapter_package");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(2);

    spinner.start();

    ros::Subscriber sub = node_handle.subscribe("move_to", 1000, move_toCallback);
    ROS_INFO("ADAPTER PACKAGE: Listening to 'move_to'.");

    ros::waitForShutdown();

    return 0;
}
